// Zasilanie Magnesów Korekcyjnych Czarnych - TM4C123GH6PM
// Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>

/* ----------------------- Includes -----------------------------------------------*/

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <FreeMODBUS/include/mbutils.h>
#include <string.h>
#include "tm4c123gh6pm.h"
#include "rw_tables.h"
#include "params.h"
#include "mbreg.h"

int main(void)
{
    /*------------------------ System Clock Configuration (50MHz) -----------------------*/

    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    /*------------------------ GPIO Pin Configuration -----------------------------------*/

    //Enable GPIO peripherals
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    //GPIO Inputs
    GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, GPIO_PIN_4);

    //GPIO Outputs
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_6 | GPIO_PIN_7);
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_5);
    GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_5);
    GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_6);

    //Default GPIO Output states
    GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6 | GPIO_PIN_7, 0);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_5, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_5);
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    //PD0-PD3, PC4-PC7 configured as inputs (default Modbus Coil state)
    GPIOPinTypeGPIOInput(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

    // UART (Modbus)
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // I2C
    GPIOPinConfigure(GPIO_PB2_I2C0SCL);
    GPIOPinConfigure(GPIO_PB3_I2C0SDA);
    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

    //SSI
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
    GPIOPinConfigure(GPIO_PA2_SSI0CLK);
    GPIOPinConfigure(GPIO_PA3_SSI0FSS);
    GPIOPinConfigure(GPIO_PA5_SSI0TX);
    GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_5);

    /*------------------------------ I2C Configuration ----------------------------------*/

     SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
     I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), FALSE); //standard speed

     /*------------------------------ I2C Timeout Timer ----------------------------------*/

     SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
     TimerConfigure(TIMER1_BASE, TIMER_CFG_ONE_SHOT);
     IntEnable(INT_TIMER1A);
     TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);

    /*------------------------------- SSI Configuration ---------------------------------*/

    SSIConfigSetExpClk(SSI0_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, 8000000, 8);
    SSIEnable(SSI0_BASE);

    /*------------------------------ MODBUS Initialization ------------------------------*/

    eMBInit(MB_MODE, MB_SLAVEID, MB_PORT, MB_BAUDRATE, MB_PARITY);
    eMBEnable();

    /*------------------------------ PSU Initialization ---------------------------------*/

    while (1)
    {
        //Modbus stack
        (void) eMBPoll();
    }
}

/*------------------------------------ MODBUS Callback Functions -----------------------------------------*/

/* Callback function - Read/Write Holding Register */
eMBErrorCode eMBRegHoldingCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usBuffer;

    usAddress -= 1;

    if ((usAddress + 1) >= (REG_8BIT_START + 1) && (usAddress + (usNRegs - 1)) <= REG_I2C)
    {
        switch (eMode)
        {
            case MB_REG_WRITE:

                //PD0-PD3, PC4-PC7 configured as outputs
                GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
                GPIOPinTypeGPIOOutput(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

                while (usNRegs > 0)
                {
                    usBuffer = *pucRegBuffer++ << 8;
                    usBuffer |= *pucRegBuffer++;

                    xWriteAddress(usAddress);

                    if (usAddress <= REG_8BIT_END) //8-bit table
                    {
                        xWrite8BitTable((UCHAR) (usBuffer & 0xFF));
                    }
                    else if (usAddress <= REG_16BIT_END) //16-bit table
                    {
                        xWrite16BitTable(usBuffer);
                    }
                    else if (usAddress == REG_I2C) //I2C
                    {
                        xWriteI2C((UCHAR) (usBuffer & 0xFF));
                    }

                    usAddress++;
                    usNRegs--;
                }
                break;

            case MB_REG_READ:

                //PD0-PD3, PC4-PC7 configured as inputs
                GPIOPinTypeGPIOInput(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
                GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

                while (usNRegs > 0)
                {
                    xWriteAddress(usAddress);

                    if (usAddress <= REG_8BIT_END) //8-bit table
                    {
                        *pucRegBuffer++ = 0;
                        *pucRegBuffer++ = xRead8BitTable();
                    }
                    else if (usAddress <= REG_16BIT_END) //16-bit table
                    {
                        usBuffer = xRead16BitTable();
                        *pucRegBuffer++ = (UCHAR) (usBuffer >> 8);
                        *pucRegBuffer++ = (UCHAR) (usBuffer & 0xFF);
                    }
                    else if (usAddress == REG_I2C) //I2C
                    {
                        *pucRegBuffer++ = 0;
                        *pucRegBuffer++ = xReadI2C();
                    }

                    usAddress++;
                    usNRegs--;
                }
                break;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

/* Callback function - Read/Write Coils */
eMBErrorCode eMBRegCoilsCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils, eMBRegisterMode eMode)
{
    return MB_ENOREG;
}

/* Callback function - Read Discrete Inputs */
eMBErrorCode eMBRegDiscreteCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete)
{
    return MB_ENOREG;
}

/* Callback function - Read Input Register */
eMBErrorCode eMBRegInputCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
    return MB_ENOREG;
}

void LEDStatus(BOOL bStatus)
{
    if (bStatus) GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_6, GPIO_PIN_6); //switch ON LED
    else GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_6, 0); //switch OFF LED
}

