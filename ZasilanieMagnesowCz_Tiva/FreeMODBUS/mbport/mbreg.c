#pragma diag_suppress 1238 //suppress warning message

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/include/mbutils.h>
#include "mbreg.h"

static BOOL bCoilWritten[(REG_COILS_NREGS + (CHAR_BIT - 1)) / CHAR_BIT] = { FALSE };
static BOOL bHoldingRegisterWritten[REG_HOLDING_NREGS] = { FALSE };

/*------------------------------------ MODBUS Callback Functions -----------------------------------------*/

/* Callback function - Read/Write Coils */
eMBErrorCode eMBRegCoilsCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= REG_COILS_START) && (usAddress + usNCoils <= REG_COILS_START + REG_COILS_NREGS))
    {
        usRegIndex = (USHORT) (usAddress - REG_COILS_START);

        switch (eMode)
        {
            case MB_REG_READ:
                while (usNCoils > 0)
                {
                    UCHAR ucResult = xMBUtilGetBits(ucMBCoils, usRegIndex, 1);
                    xMBUtilSetBits(pucRegBuffer, usRegIndex - (usAddress - REG_COILS_START), 1, ucResult);
                    usRegIndex++;
                    usNCoils--;
                }
                break;

            case MB_REG_WRITE:
                while (usNCoils > 0)
                {
                    UCHAR ucResult = xMBUtilGetBits(pucRegBuffer, usRegIndex - (usAddress - REG_COILS_START), 1);
                    xMBUtilSetBits(ucMBCoils, usRegIndex, 1, ucResult);
                    xMBUtilSetBits(bCoilWritten, usRegIndex, 1, 1);
                    usRegIndex++;
                    usNCoils--;
                }
                break;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

/* Callback function - Read Discrete Inputs */
eMBErrorCode eMBRegDiscreteCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= REG_DISCRETE_START) && (usAddress + usNDiscrete <= REG_DISCRETE_START + REG_DISCRETE_NREGS))
    {
        usRegIndex = (USHORT) (usAddress - REG_DISCRETE_START);

        while (usNDiscrete > 0)
        {
            UCHAR ucResult = xMBUtilGetBits(ucMBDiscretes, usRegIndex, 1);
            xMBUtilSetBits(pucRegBuffer, usRegIndex - (usAddress - REG_DISCRETE_START), 1, ucResult);
            usRegIndex++;
            usNDiscrete--;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

/* Callback function - Read Input Register */
eMBErrorCode eMBRegInputCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= REG_INPUT_START) && (usAddress + usNRegs <= REG_INPUT_START + REG_INPUT_NREGS))
    {
        usRegIndex = (USHORT) (usAddress - REG_INPUT_START);

        while (usNRegs > 0)
        {
            *pucRegBuffer++ = (UCHAR) (usMBInputReg[usRegIndex] >> 8);
            *pucRegBuffer++ = (UCHAR) (usMBInputReg[usRegIndex] & 0xFF);
            usRegIndex++;
            usNRegs--;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}


/* Callback function - Read/Write Holding Register */
eMBErrorCode eMBRegHoldingCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= REG_HOLDING_START) && (usAddress + usNRegs <= REG_HOLDING_START + REG_HOLDING_NREGS))
    {
        usRegIndex = (USHORT) (usAddress - REG_HOLDING_START);
        switch (eMode)
        {
            /* Pass current register values to the protocol stack. */
            case MB_REG_READ:
                while (usNRegs > 0)
                {
                    *pucRegBuffer++ = (UCHAR)(usMBHoldingReg[usRegIndex] >> 8);
                    *pucRegBuffer++ = (UCHAR)(usMBHoldingReg[usRegIndex] & 0xFF);
                    usRegIndex++;
                    usNRegs--;
                }
                break;

            /* Update current register values with new values from the
             * protocol stack. */
            case MB_REG_WRITE:
                while (usNRegs > 0)
                {
                    usMBHoldingReg[usRegIndex] = *pucRegBuffer++ << 8;
                    usMBHoldingReg[usRegIndex] |= *pucRegBuffer++;
                    bHoldingRegisterWritten[usRegIndex] = TRUE;
                    usRegIndex++;
                    usNRegs--;
                }
                break;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

/* Returns TRUE if coil value is new */
BOOL bMBCoilIsWritten(UCHAR ucIndex)
{
    if (bCoilWritten[ucIndex - 1])
    {
        bCoilWritten[ucIndex - 1] = FALSE;
        return TRUE;
    }

    return FALSE;
}

/* Returns TRUE if holding register value is new */
BOOL bMBHoldingRegIsWritten(UCHAR ucIndex)
{
    if (bHoldingRegisterWritten[ucIndex - 1])
    {
        bHoldingRegisterWritten[ucIndex - 1] = FALSE;
        return TRUE;
    }

    return FALSE;
}
