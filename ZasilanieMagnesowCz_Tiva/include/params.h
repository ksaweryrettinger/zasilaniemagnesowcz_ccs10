#ifndef INCLUDE_PARAMS_H_
#define INCLUDE_PARAMS_H_

/* ----------------------- MODBUS Parameters ------------------------------------*/

#define MB_MODE              (MB_RTU)
#define MB_SLAVEID           (0x01)
#define MB_PORT              (0)
#define MB_BAUDRATE          (115200)
#define MB_PARITY            (MB_PAR_EVEN)

/* ----------------------- I2C Configuration ------------------------------------*/

#define ADDRESS_PCF8574      (0b0100111)
#define I2C_SPEED            (100000)
#define I2C_MASTER_TIMEOUT   (15000)    //equivalent to 30 I2C clock cycles

/* ----------------------- External Register Addressing -------------------------*/

#define REG_8BIT_START      (0)
#define REG_8BIT_END        (1023)
#define REG_16BIT_START     (1024)
#define REG_16BIT_END       (2047)
#define REG_I2C             (2048)

/* ----------------------- Other ------------------------------------------------*/

#define SYS_DELAY_WRITE     (3) //equivalent to 180ns at 50MHz clock speed
#define SYS_DELAY_READ      (2) //equivalent to 120ns at 50MHz clock speed

#endif /* INCLUDE_PARAMS_H_ */
