#pragma once

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <FreeMODBUS/include/mbutils.h>
#include "tm4c123gh6pm.h"
#include "params.h"

/* ----------------------- Function prototypes ------------------------------------*/

void xI2CMasterWait(ULONG*);
void xWriteI2C(UCHAR ucData);
void xWriteAddress(USHORT);
void xWrite8BitTable(UCHAR);
void xWrite16BitTable(USHORT);
UCHAR xReadI2C(void);
UCHAR xRead8BitTable(void);
USHORT xRead16BitTable(void);
